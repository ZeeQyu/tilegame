TileGame
========
An experimental tile-based game

INSTALL.txt has installation instructions.

Generates a game world and lets you move a character in a simple way (no borders and no collision)
Arrow keys to move. Space summons a beetle minion at your feet that randomly roams around.

//ZeeQyu